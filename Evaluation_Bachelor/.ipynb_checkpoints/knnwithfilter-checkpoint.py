from prepare import prepare_for_knn_dtw
import numpy as np


def getmin(array):
    result = []
    mini = array[0]
    index = 0
    for i in range(1, len(array)):
        if mini > array[i]:
            mini = array[i]
            index = i

    result.append(mini)
    result.append(index)
    return result


def euclidean_dist(x1, x2, y1, y2):
    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5


class knnwithfilter:

    def __init__(self, person0, person1, person2, person3, person4, person5, person00, person01, person02, person03,
                 person04, person05):
        self.data_person0 = person0
        self.data_person1 = person1
        self.data_person2 = person2
        self.data_person3 = person3
        self.data_person4 = person4
        self.data_person5 = person5
        self.data_person0_2 = person00
        self.data_person1_2 = person01
        self.data_person2_2 = person02
        self.data_person3_2 = person03
        self.data_person4_2 = person04
        self.data_person5_2 = person05

    def getprepareforknn(self):
        prepare = prepare_for_knn_dtw(self.data_person0, self.data_person1, self.data_person2, self.data_person3,
                                      self.data_person4,
                                      self.data_person5,
                                      self.data_person0_2, self.data_person1_2, self.data_person2_2,
                                      self.data_person3_2,
                                      self.data_person4_2,
                                      self.data_person5_2)
        return prepare

    
    def runknn_algorithm(self, scene):
        data = [self.data_person0_2, self.data_person1_2, self.data_person2_2, self.data_person3_2,
                self.data_person4_2, self.data_person5_2]

        for i in range(0, len(data)):
            currentperson = data[i]
            for j in range(0, 2):
                result = self.knn_algorithm(currentperson, j, 0.6, scene, i)
        return result

    def getlittlesize(array1, array2):
        result = len(array1)
        if len(array1) > len(array2):
            result = len(array2)
        return result

    
     # diese methode fuhrt knn für alle mögliche treeshold
        #array enthählt alle treeshold INT  bestimmt die shwellenwert
        #filename String : ist eine txt file und bestimmt, wo die ergebnisse gespeichert wird
        #num: bestimmmt die anzahl der datensätz für 
        
        
    def test_all_treeshold(self, array, confidence, scene, num):

        
        preparefor = self.getprepareforknn()

        person0_0, person0_1 = preparefor.changewithrolling(self.data_person0, confidence, num)
        person1_0, person1_1 = preparefor.changewithrolling(self.data_person1, confidence, num)
        person2_0, person2_1 = preparefor.changewithrolling(self.data_person2, confidence, num)
        person3_0, person3_1 = preparefor.changewithrolling(self.data_person3, confidence, num)
        person4_0, person4_1 = preparefor.changewithrolling(self.data_person4, confidence, num)
       # person5_0, person5_1 = preparefor.changewithrolling(self.data_person5, confidence, num)

        person0_2_0, person0_2_1 = preparefor.changewithrolling(self.data_person0_2, confidence, num)
        person1_2_0, person1_2_1 = preparefor.changewithrolling(self.data_person1_2, confidence, num)
        person2_2_0, person2_2_1 = preparefor.changewithrolling(self.data_person2_2, confidence, num)
        person3_2_0, person3_2_1 = preparefor.changewithrolling(self.data_person3_2, confidence, num)
        person4_2_0, person4_2_1 = preparefor.changewithrolling(self.data_person4_2, confidence, num)
        person5_2_0, person5_2_1 = preparefor.changewithrolling(self.data_person5_2, confidence, num)

        #arraydb_0 = [person0_0, person1_0, person2_0, person3_0, person4_0, person5_0]

        #arraydb_1 = [person0_1, person1_1, person2_1, person3_1, person4_1, person5_1]

        
        
        
        arraydb_0 = [person0_0, person1_0, person2_0, person3_0, person4_0]

        arraydb_1 = [person0_1, person1_1, person2_1, person3_1, person4_1]

        
        arraynew_0 = [person0_2_0, person1_2_0, person2_2_0, person3_2_0, person4_2_0, person5_2_0]

        arraynew_1 = [person0_2_1, person1_2_1, person2_2_1, person3_2_1, person4_2_1, person5_2_1]

        arrayeye1 = [arraynew_1, arraydb_1]
        arrayeye0 = [arraynew_0, arraydb_0]
        arraydef = [arrayeye0, arrayeye1]

        for a in range(0, len(array)):
            currentreshold = array[a]
            #tp, tn, fp, fn = 0, 0, 0, 0

            for i in range(0, len(arraydef)):
                newarray = arraydef[i][0]
                dbarray = arraydef[i][1]
                tp, tn, fp, fn = 0, 0, 0, 0
                resultarray = []
                file = open('test_treshold_scene' + str(scene) + 'eye_' + str(i) + '.txt', "a")
                for j in range(0, len(newarray)):
                    arraydistance = []
                   
                    currentperson = np.array(
                        newarray[j][
                            ['pupil_timestamp', 'diameter', 'eye_id', 'confidence', 'diameter_median_filtered']])
                    size = len(currentperson)
                    for k in range(0, len(dbarray)):
                        # arraydistance = []

                        dbperson = np.array(
                            dbarray[k][
                                ['pupil_timestamp', 'diameter', 'eye_id', 'confidence', 'diameter_median_filtered']])

                        if size > len(dbperson):
                            size = len(dbperson)
                        distance = 0
                        for l in range(num, size):
                            distance += abs(euclidean_dist(currentperson[l][0], dbperson[l][0], currentperson[l][1],
                                                           dbperson[l][1]))
                        # resultarray.append({'currentperson': j, 'dbperson': k, 'distance': distance, 'eyeid': i})
                        predict = j
                        arraydistance.append(distance)
                    mini = getmin(arraydistance)
                    print(mini)
                    if mini[0] < currentreshold and mini[1] == predict:
                        arraydistance.append(
                            {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                             'eyeid': i, 'true positif': 'tp'})
                        tp += 1
                    elif mini[0] < currentreshold and mini[1] != predict:
                        arraydistance.append(
                            {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                             'eyeid': i, 'false positif': 'fp'})
                        fp += 1
                    elif mini[0] > currentreshold and mini[1] not in [0, 1, 2, 3, 4]:
                        arraydistance.append(
                            {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                             'eyeid': i, 'false negatif': 'fn'})
                        fn += 1
                    elif mini[0] > currentreshold and mini[1] in [0, 1, 2, 3, 4]:
                        arraydistance.append(
                            {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                             'eyeid': i, 'true negatif ': 'tn'})
                        tn += 1

                accuracy = (tp + tn) / (tp + fp + fn + tn)
                resultarray.append(
                        {'scene': scene,'eye ': i,'minitreshohld ': mini[0], 'treeshold': currentreshold, 'tp': tp, 'fp': fp, 'fn': fn, 'tn': tn,
                         'accuracy': accuracy})
                    
                print({'scene': scene, 'eye ': i,'minitreshohld ': mini[0],'treeshold': currentreshold, 'tp': tp, 'fp': fp, 'fn': fn, 'tn': tn,
                         'accuracy': accuracy})
                    #resultarray.append(arraydistance)
                print("---------------")
                    
                file.write(str(resultarray))

        return arraydistance

    def newknn_algorithmus(self, confidence, treeshold, scene, num):

        resultarray = []
        preparefor = self.getprepareforknn()
        file = open('newknn_' + str(scene) + '.txt', "a")

        person0_0, person0_1 = preparefor.changewithrolling(self.data_person0, confidence, num)
        person1_0, person1_1 = preparefor.changewithrolling(self.data_person1, confidence, num)
        person2_0, person2_1 = preparefor.changewithrolling(self.data_person2, confidence, num)
        person3_0, person3_1 = preparefor.changewithrolling(self.data_person3, confidence, num)
        person4_0, person4_1 = preparefor.changewithrolling(self.data_person4, confidence, num)
        person5_0, person5_1 = preparefor.changewithrolling(self.data_person5, confidence, num)

        person0_2_0, person0_2_1 = preparefor.changewithrolling(self.data_person0_2, confidence, num)
        person1_2_0, person1_2_1 = preparefor.changewithrolling(self.data_person1_2, confidence, num)
        person2_2_0, person2_2_1 = preparefor.changewithrolling(self.data_person2_2, confidence, num)
        person3_2_0, person3_2_1 = preparefor.changewithrolling(self.data_person3_2, confidence, num)
        person4_2_0, person4_2_1 = preparefor.changewithrolling(self.data_person4_2, confidence, num)
        person5_2_0, person5_2_1 = preparefor.changewithrolling(self.data_person5_2, confidence, num)

        #arraydb_0 = [person0_0, person1_0, person2_0, person3_0, person4_0, person5_0]
        arraydb_0 = [person0_0, person1_0, person2_0, person3_0, person4_0]

       #arraydb_1 = [person0_1, person1_1, person2_1, person3_1, person4_1, person5_1]
        arraydb_1 = [person0_1, person1_1, person2_1, person3_1, person4_1]

        arraynew_0 = [person0_2_0, person1_2_0, person2_2_0, person3_2_0, person4_2_0, person5_2_0]

        arraynew_1 = [person0_2_1, person1_2_1, person2_2_1, person3_2_1, person4_2_1, person5_2_1]

        arrayeye1 = [arraynew_1, arraydb_1]
        arrayeye0 = [arraynew_0, arraydb_0]
        arraydef = [arrayeye0, arrayeye1]

        for i in range(0, len(arraydef)):
            newarray = arraydef[i][0]
            dbarray = arraydef[i][1]

            for j in range(0, len(newarray)):
                arraydistance = []
                tp, tn, fp, fn = 0, 0, 0, 0
                currentperson = np.array(
                    newarray[j][['pupil_timestamp', 'diameter', 'eye_id', 'confidence', 'diameter_median_filtered']])
                size = len(currentperson)
                for k in range(0, len(dbarray)):
                    # arraydistance = []

                    dbperson = np.array(
                        dbarray[k][['pupil_timestamp', 'diameter', 'eye_id', 'confidence', 'diameter_median_filtered']])

                    if size > len(dbperson):
                        size = len(dbperson)
                    distance = 0
                    for l in range(53, size):
                        distance += abs(euclidean_dist(currentperson[l][0], dbperson[l][0], currentperson[l][1],
                                                       dbperson[l][1]))
                    # resultarray.append({'currentperson': j, 'dbperson': k, 'distance': distance, 'eyeid': i})
                    predict = j
                    arraydistance.append(distance)
                mini = getmin(arraydistance)
                print(mini)
                if mini[0] < treeshold and mini[1] == predict:
                    arraydistance.append(
                        {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                         'eyeid': i, 'true positif': 'tp'})
                    tp += 1
                elif mini[0] < treeshold and mini[1] != predict:
                    arraydistance.append(
                        {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                         'eyeid': i, 'false positif': 'fp'})
                    fp += 1
                elif mini[0] > treeshold and mini[1] not in [0, 1, 2, 3, 4]:
                    arraydistance.append(
                        {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                         'eyeid': i, 'false negatif': 'fn'})
                    fn += 1
                elif mini[0] > treeshold and mini[1] in [0, 1, 2, 3, 4]:
                    arraydistance.append(
                        {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0],
                         'eyeid': i, 'true negatif ': 'tn'})
                    tn += 1

                # print(arraydistance)
                resultarray.append(arraydistance)
                print("---------------")
                resultarray.append(
                    {'authenticate person is': mini[1], 'predicted person': predict, 'distance': mini[0], 'eyeid': i})
                resultarray.append(
                    {'------------------------------new person------------------------------------------in eye_': i})

        file.write(str(resultarray))

        return arraydistance
