from scipy.spatial.distance import euclidean
from dtw import *

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

class execute_dtw:

    def __init__(self, db, data, treeshold):
        self.db = db
        self.data = data
        self.treeshold = treeshold

    def runddtw(self, eyeid, filename):

        tp, tn, fp, fn = 0, 0, 0, 0
        currentperson = 0
        result = []
        file = open('newdtw'+filename, "a")
        all_treeshold = []
        resultperson = []
        for i in range(0, len(self.data)):
            currentperson = i
            resultperson = []
            arraydistance = []

            file.write("this contains the result of dtw analyse for eye " + str(eyeid))
            #for j in range(0, len(self.db)):
            for j in range(0, 10000):
                print("debut")
                distance, cost_matrix, acc_cost_matrix, path = dtw(self.data[i], self.db[j], dist=euclidean)
                all_treeshold.append(distance)
                print(distance)
                arraydistance.append({'distance': distance, 'person': j})
                resultperson.append({'the person': i, 'has a distance of': distance, 'to person ': j})
                file.write(str({'the person': i, 'has a euclidean distance of': distance, 'to person ': j}))
                file.write("\n")

            minimum = arraydistance[0]['distance']
            label = 0
            for k in range(1, len(arraydistance)):
                if minimum > arraydistance[k]['distance']:
                    minimum = arraydistance[k]['distance']
                    label = arraydistance[k]['person']

            if minimum < self.treeshold and label == i:
                resultperson.append({'the authenticated person is': label, 'true positif': 'tp'})
                tp += 1
            elif minimum < self.treeshold and label != i:
                resultperson.append({'the authenticated person is': label, 'false positif': 'fp'})
                fp += 1
            elif minimum > self.treeshold and currentperson not in [0, 1, 2, 3, 4]:
                resultperson.append({'the authenticated person is': label, 'false negatif': 'fn'})
                fn += 1
            elif minimum > self.treeshold and currentperson in [0, 1, 2, 3, 4]:
                resultperson.append({'the authenticated person is': label, 'true negatif ': 'tn'})
                tn += 1
            result.append(resultperson)
            result.append({'----------------------------------------'})
        accuracy = (tp + tn) / (tp + fp + fn + tn)
        result.append(accuracy)
        file.write(str(result))
        file.write("\n")
        file.write("\n")

        return result, all_treeshold

    def Change_timestamp_filterconfidenceAndeye(data_person, confidence):
        arrayresult = []
        arrayeye0 = []
        arrayeye1 = []
        datapersonreader = pd.read_csv(data_person)
        # datapersonreader.loc[datapersonreader.confidence < 0.6, ['diameter']] = np.nan
        arrayperson = np.array(datapersonreader[['pupil_timestamp', 'diameter', 'eye_id', 'confidence']])
        arrayperson['pupil_timestamp'] = arrayperson['pupil_timestamp'].rolling(2, win_type='triang').sum()
        time = 0.0
        gutconfidence = confidence
        size = len(arrayperson)
        for i in range(0, size):
            time += 0.001
            if arrayperson[i][3] >= gutconfidence:
                if arrayperson[i][2] == 0.0:
                    arrayeye0.append({'time': time, 'diameter': arrayperson[i][1], 'eye_id': arrayperson[i][2],
                                      'confidence': arrayperson[i][3], 'changeTime': 'no'})
                else:
                    arrayeye1.append({'time': time, 'diameter': arrayperson[i][1], 'eye_id': arrayperson[i][2],
                                      'confidence': arrayperson[i][3], 'changeTime': 'no'})

        arrayresult.append(arrayeye0)
        arrayresult.append(arrayeye1)

        return arrayresult

