﻿using System.Collections;
using UnityEngine;

namespace HCI 
{
    public class AtomDialogDelay : AtomDialog
    {
        private const int DELAY_IN_SECONDS = 5;

        private int delay;

        public AtomDialogDelay(StudyManager studyManager, string message, int delay = DELAY_IN_SECONDS) : base(studyManager, message)
        {
            this.delay = delay;
        }

        public override void Start()
        {
            base.Start();
            this.SetActive(true);

            this.studyManager.StartCoroutine(WaitForDelay());
        }

        IEnumerator WaitForDelay()
        {
            yield return new WaitForSeconds(this.delay);
            this.SetActive(false);
            this.hasFinished = true;
            yield return null;
        }

        public override void OnSliderChange()
        {
        }

        public override void OnButtonPressed()
        {
            
        }
    }
}